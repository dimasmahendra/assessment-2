<?php
/**
 * functions and definitions
 *
 * 
 */

/**
 * Enqueue scripts and styles.
 */
function dimas_scripts() {
	wp_enqueue_script( 'jquery-3.3.1', 'http://code.jquery.com/jquery-3.3.1.min.js', array(), null, false );
    wp_enqueue_script( 'dimas-js', get_template_directory_uri() . '/source/dimas.js', array(), null, true );
    wp_enqueue_style( 'dimas-css', get_template_directory_uri() . '/source/dimas.css', null, false );
}
add_action( 'wp_enqueue_scripts', 'dimas_scripts' );


/*Add Dynamics Menu*/
function wpb_custom_new_menu() {
  register_nav_menus(
    array(
      'dimas-menu' => __( 'My Custom Menu' )
    )
  );
}
add_action( 'init', 'wpb_custom_new_menu' );